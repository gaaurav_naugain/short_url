class CreateUrls < ActiveRecord::Migration[7.0]
  def change
    create_table :urls do |t|
      t.string :full_url
      t.string :short_url
      t.datetime :expires_at
      t.integer :click

      t.timestamps
    end
  end
end
