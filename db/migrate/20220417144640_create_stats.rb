class CreateStats < ActiveRecord::Migration[7.0]
  def change
    create_table :stats do |t|
      t.string :ip
      t.string :country
      t.references :url, null: false, foreign_key: true

      t.timestamps
    end
  end
end
