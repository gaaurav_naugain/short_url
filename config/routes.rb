Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "home#index"
  get 'stats' => "home#stats"
  post 'shorten' => "home#shorten"
  get ':id' => "home#decrypt_url"
end
