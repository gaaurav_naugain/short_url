class Url < ApplicationRecord
  has_many :stats
  before_create :add_short_url

  def add_short_url
    self.short_url = SecureRandom.alphanumeric(5)
  end
end
