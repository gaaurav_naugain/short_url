require 'securerandom'
class HomeController < ApplicationController

  def index
  end


  def stats
    @urls = Url.includes(:stats).all
  end

  def shorten
    url = Url.create(full_url: params[:url], expires_at: Time.now + 1.month)
    @shorterned_url =  "http://localhost:3001/#{url.short_url}"
    respond_to do |format|
      format.turbo_stream
    end
  end

  def decrypt_url
    url = Url.find_by(short_url: params[:id])
    if url.present? && url.expires_at > Time.now
      url.update(click: url.click.to_i + 1)
      ip = request.ip
      url.stats.create(ip: ip, country: Geocoder.search(ip).first.country)
      redirect_to url.full_url, allow_other_host: true
    else
      render :file => "#{Rails.root}/public/404.html"
    end
  end
end
